import { request, RequestOptions } from "http";

export function fetchJson(options: RequestOptions) {
  return new Promise(resolve => {
    const req = request(
      {
        ...options
      },
      res => {
        let data = "";
        res.on("data", chunk => (data += chunk));
        res.on("end", () => resolve(JSON.parse(data)));
      }
    );
    req.on("error", e => resolve({ error: e, ...options }));
    req.end();
  });
}
