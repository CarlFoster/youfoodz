export enum PrivateServices {
  rentalA = "rentalA",
  rentalB = "rentalB"
}

export enum PublicServices {
  quoting = "quoting",
}

export const SERVICE_CONFIG = {
  [PrivateServices.rentalA]: {
    port: 80,
    path: 'rentalA',
  },
  [PrivateServices.rentalB]: {
    port: 80,
    path: 'rentalB',
  },
  [PublicServices.quoting]: {
    port: 80,
    path: 'quoting'
  }
}