import server from "server";
import { get } from "server/router";
import { PrivateServices, SERVICE_CONFIG } from "../config";
import { fetchJson } from "../common";

export default function start() {
  const { port } = SERVICE_CONFIG[PrivateServices.rentalA];
  const app = server({ port }, get("/", getQuote));
  return app;
}

async function getQuote() {
  const quote = await fetchJson({
    hostname: 'api.myjson.com',
    path: '/bins/7c0qw',
  })
  return quote;
}
