import server from "server";
import { get } from "server/router";
import { SERVICE_CONFIG, PrivateServices } from "../config";

export default function start() {
  const { port } = SERVICE_CONFIG[PrivateServices.rentalB];
  const app = server({ port }, get("/", getQuote));
  return app;
}

function getQuote() {
  const quote = {
    name: "Service B Rental",
    currency: "AUD",
    amount: (30).toFixed(2)
  };

  return { quote };
}
