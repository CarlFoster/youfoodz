import server from "server";
import { get } from "server/router";
import { PrivateServices, SERVICE_CONFIG, PublicServices } from "../config";
import { fetchJson } from "../common";

export default function start() {
  const { port } = SERVICE_CONFIG[PublicServices.quoting];
  const app = server({ port }, get("/", getQuotes));
  return app;
}

async function getQuotes() {
  const quotes = await Promise.all(
    Object.values(PrivateServices).map(service =>
      fetchLocalService(service)
        .then(({ quote }) => quote)
        .then(addFee)
    )
  );

  return { quotes };
}

function addFee(quote: Quote) {
  const amount = Number(quote.amount);
  const commission = amount * 0.15;

  return {
    ...quote,
    commission: commission.toFixed(2),
    total: (amount + commission).toFixed(2)
  };
}

interface QuoteResponse {
  quote: Quote;
}

interface Quote {
  name: string;
  currency: string;
  amount: string;
}
async function fetchLocalService(
  service: PrivateServices
): Promise<QuoteResponse> {
  const data = await fetchJson({hostname: service});
  return data as QuoteResponse;
}
