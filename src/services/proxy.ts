import server from "server";
import { get, post } from "server/router";
import { PrivateServices, PublicServices, SERVICE_CONFIG } from "../config";
import { fetchJson } from "../common";

export default function start() {
  const app = server({ port: 8080 },
    get('/', () => fetchJson({hostname: PublicServices.quoting})),
    // For testing:
    // ...proxy(PrivateServices.rentalA),
    // ...proxy(PrivateServices.rentalB),
    // ...proxy(PublicServices.quoting),
  );

  return app;
}

function proxy(service: string) {
  const config = SERVICE_CONFIG[service as PrivateServices | PublicServices];
  return [
    get(`/${config.path}`, () => fetchJson({hostname: config.path})),
    post(`/${config.path}`, () => fetchJson({hostname: config.path, method: 'POST'}))
  ];
}
