const serviceToRun = process.env.SERVICE_TO_START
const start = require(`./services/${serviceToRun}`).default

if (typeof start !== 'function') {
  throw new Error('Service not implemented')
}

start()