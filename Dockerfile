FROM node:10 as builder

WORKDIR /sync/

COPY yarn.lock package.json tsconfig.json ./
RUN yarn
COPY src ./src
RUN yarn compile

FROM node:10
RUN yarn global add pm2

WORKDIR /app/
COPY package.json yarn.lock ./
ENV NODE_ENV='production'
RUN yarn
COPY --from=builder /sync/dist ./dist

CMD ["pm2-runtime", "--watch", "dist/index.js"]