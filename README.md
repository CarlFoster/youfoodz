# Quoting

Node.js Microservices in a monorepo using `docker swarm` to replicate and scale. This will scale very well horizontally as more services and nodes can be added to the swarm at any time.

## Building

`docker build . -t youfoodz:latest`

## Running

Can be run a couple of ways although currently set up for docker swarm:
* docker stack deploy youfoodz --compose-file docker-compose.yml
  * ensure that docker swarm is configured (`docker swarm init`)
* docker-compose up -d --build
  * ensure to comment out the `deploy` keys in the compose file

Application can then be accessed from 127.0.0.1:8080

## Thoughts

I didn't really understand what was meant for RentalCompanyA. I just assumed that it accessed the JSON api that was mentioned.

There isn't really a need for the proxy, but it would make it easier for accessing the private services in a test environment.
